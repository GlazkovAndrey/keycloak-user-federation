package com.social.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(
        @NamedQuery(name = User.FIND_USER, query = "select u from User u where u.login = :login")
)

@Entity
@Table(name = "POL_PERSONAL")
public class User {
    public static final String FIND_USER = "User.findUser";

    @Id
    @Column(name = "ID_PERSON")
    private Long id;

    @Column(name = "LOGIN_P")
    private String login;

    @Column(name = "PASSW_P")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
