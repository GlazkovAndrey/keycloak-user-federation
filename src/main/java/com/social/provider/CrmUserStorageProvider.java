package com.social.provider;

import com.social.ejb.UserEJB;
import com.social.model.User;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.credential.CredentialModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.adapter.AbstractUserAdapter;
import org.keycloak.storage.user.UserLookupProvider;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CrmUserStorageProvider implements UserStorageProvider,
                                        UserLookupProvider,
                                        CredentialInputValidator,
                                        CredentialInputUpdater {
    protected KeycloakSession session;
    protected ComponentModel model;
    protected UserEJB userEJB;

    protected Map<String, UserModel> loaderUser = new HashMap<>();

    public CrmUserStorageProvider(KeycloakSession session, ComponentModel model, UserEJB userEJB) {
        this.session = session;
        this.model = model;
        this.userEJB = userEJB;
    }

    @Override
    public boolean updateCredential(RealmModel realmModel, UserModel userModel, CredentialInput credentialInput) {
        return false;
    }

    @Override
    public void disableCredentialType(RealmModel realmModel, UserModel userModel, String s) {
    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realmModel, UserModel userModel) {
        return Collections.emptySet();
    }

    @Override
    public boolean supportsCredentialType(String s) {
        return s.equals(CredentialModel.PASSWORD);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realmModel, UserModel userModel, String s) {
        User user = userEJB.findUserByLogin(userModel.getUsername());
        if (user == null) return false;
        String password = user.getPassword();
        return s.equals(CredentialModel.PASSWORD) && password != null && !password.isEmpty();
    }

    @Override
    public boolean isValid(RealmModel realmModel, UserModel userModel, CredentialInput credentialInput) {
        if (!supportsCredentialType(credentialInput.getType()) || !(credentialInput instanceof CredentialModel))
            return false;
        UserCredentialModel credentialModel = (UserCredentialModel) credentialInput;
        User user = userEJB.findUserByLogin(userModel.getUsername());
        if (user == null) return false;
        String password = user.getPassword();
        return (password != null && !password.isEmpty() && password.equals(credentialModel.getValue()));
    }

    @Override
    public void close() {

    }

    @Override
    public UserModel getUserById(String s, RealmModel realmModel) {
        StorageId storageId = new StorageId(s);
        String username = storageId.getExternalId();
        return getUserByUsername(username, realmModel);    }

    @Override
    public UserModel getUserByUsername(String s, RealmModel realmModel) {
        UserModel adapter = loaderUser.get(s);
        if (adapter == null) {
            User user = userEJB.findUserByLogin(s);
            String password = user.getPassword();
            adapter = new AbstractUserAdapter(session, realmModel, model) {
                @Override
                public String getUsername() {
                    return s;
                }
            };
            loaderUser.put(s, adapter);
        }
        return adapter;
    }

    @Override
    public UserModel getUserByEmail(String s, RealmModel realmModel) {
        return null;
    }
}
