package com.social.provider;

import com.social.ejb.UserEJB;
import org.keycloak.Config;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.storage.UserStorageProviderFactory;

public class CrmUserStorageProviderFactory implements UserStorageProviderFactory<CrmUserStorageProvider> {
    public static final String PROVIDER_ID = "crm-user-provider";
    private UserEJB userEJB;

    @Override
    public void init(Config.Scope config) {
        userEJB = new UserEJB();
    }

    @Override
    public CrmUserStorageProvider create(KeycloakSession keycloakSession, ComponentModel componentModel) {
        return new CrmUserStorageProvider(keycloakSession, componentModel, userEJB);
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
