package com.social.ejb;

import com.social.model.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

@Stateless
@LocalBean
public class UserEJB {

    @PersistenceContext(unitName = "SocialDS")
    EntityManager em;

    public UserEJB() {
    }

    public User findUserByLogin(@NotNull String login) {
        return em.createNamedQuery(User.FIND_USER, User.class)
                .setParameter("login", login)
                .getSingleResult();
    }
}
